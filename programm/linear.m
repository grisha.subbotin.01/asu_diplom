% Oпределение угловых температур
top_left = 25;      % верхний левый угол комнаты
top_right = 25;     % верхний правый угол комнаты
bottom_left = 25;   % нижний левый угол комнаты
bottom_right = 25;  % нижний правый угол комнаты
center = 0;         % центральная температура

% Определение размеров комнаты
width = 10;     % ширина комнаты
height = 10;    % высота комнаты

% Определение разрешения температурной карты
resolution = 1;    % разрешение (шаг) сетки

% Создание сетки для температурной карты
x = 0:resolution:width;
y = 0:resolution:height;

% Создание матрицы для хранения температур
temperature_map = zeros(length(y), length(x));

% Установка температуры в центре комнаты
center_x = width / 2;
center_y = height / 2;
center_index_x = round(center_x / resolution);
center_index_y = round(center_y / resolution);
temperature_map(center_index_y, center_index_x) = center;

% Линейная интерполяция для определения температур внутри комнаты
for i = 1:length(y)
    for j = 1:length(x)
        x_value = x(j);
        y_value = y(i);
        x_distance = abs(x_value - center_x);
        y_distance = abs(y_value - center_y);
        max_x_distance = center_x;
        max_y_distance = center_y;
        x_ratio = x_distance / max_x_distance;
        y_ratio = y_distance / max_y_distance;
        % Получение инвертированного значения температуры
        temperature = top_left * max(x_ratio, y_ratio);
        temperature_map(i, j) = temperature;
    end
end

% Создание более плотной сетки для интерполяции
[X, Y] = meshgrid(x, y);
[Xq, Yq] = meshgrid(0:resolution/10:width, 0:resolution/10:height);

% Интерполяция с использованием функции interp2
temperature_map_interpolated = interp2(X, Y, temperature_map, Xq, Yq);

% Визуализация температурной карты
imagesc(Xq(1,:), Yq(:,1), temperature_map_interpolated);
colorbar;
title('Температурная карта Octave');
xlabel('Позиция по горизонтали');
ylabel('Позиция по вертикали');

