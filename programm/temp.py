import numpy as np
import matplotlib.pyplot as plt
# TestSmoothBivariateSpline::test_integral
from scipy.interpolate import SmoothBivariateSpline, LinearNDInterpolator

plt.rcParams['figure.figsize'] = [10.0, 10.0]
plt.rcParams['figure.dpi'] = 300
plt.rcParams['font.family'] = 'monospace'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['font.size'] = 9
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
plt.rcParams['xtick.bottom'] = True
plt.rcParams['xtick.labelbottom'] = True
plt.rcParams['xtick.top'] = False
plt.rcParams['xtick.labeltop'] = False

def plotTemperature(matrixT, size):
    fig, ax = plt.subplots()
    plt.title("Температурная карта")
    cax = ax.matshow(matrixT, vmin = 0, vmax = 25, cmap=plt.cm.plasma, interpolation='nearest')
    im_ratio = matrixT.shape[0] / matrixT.shape[1]
    fig.colorbar(cax, fraction=0.047*im_ratio)
    ax.invert_yaxis()
    ax.xaxis.tick_bottom()

    for i in range(size):
        for j in range(size):
            if i == j:
                c = int(matrixT[j,i])
                if c <= 10:
                    ax.text(i, j, str(c), va='center', ha='center', color='white')
                else:
                    ax.text(i, j, str(c), va='center', ha='center', color='black')

    fileName = "Temperature.png"
    plt.savefig(fileName)
    plt.close()

x = np.array([1,1,3,5,5])
y = np.array([1,5,3,1,5])
z = np.array([25,25,0,25,25])

xy = np.c_[x, y]
lut2 = LinearNDInterpolator(xy, z)

X = np.linspace(min(x), max(x))
Y = np.linspace(min(y), max(y))
X, Y = np.meshgrid(X, Y)

result = lut2(X, Y)

plotTemperature(result, 50)
fig = plt.figure()
ax = fig.add_subplot(projection='3d')

ax.plot_wireframe(X, Y, lut2(X, Y))
ax.scatter(x, y, z,  'o', color='k', s=48)
