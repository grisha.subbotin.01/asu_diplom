import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.interpolate import griddata
import matplotlib.cm as cm

def update_plot(sensor_data, ax, ax2, fig, cbar2=None, cbar=None):

    ax.clear()
    
    x = sensor_data[:, 2]
    y = sensor_data[:, 3]
    z = sensor_data[:, 4]
    t = sensor_data[:, 1]

    x_grid, y_grid = np.meshgrid(
        np.linspace(min(x), max(x), num=30),
        np.linspace(min(y), max(y), num=30)
    )

    z_grid = griddata((x, y), z, (x_grid, y_grid), method='cubic')
    t_grid = griddata((x, y), t, (x_grid, y_grid), method='cubic')

    sc = ax.plot_surface(x_grid, y_grid, z_grid, facecolors=plt.cm.coolwarm(t_grid/np.max(t)), cmap='coolwarm', antialiased=False)
    sc.set_clim([np.min(t),np.max(t)])
    
    im = ax2.imshow((t_grid/np.max(t)), cmap=cm.coolwarm, extent=[min(x), max(x), min(y), max(y)], origin='lower')
    
    if cbar2 is not None:
        cbar2.remove()

    cbar2 = fig.colorbar(sc, ax=ax2)
    cbar2.set_label('Температура (°C)')

    #cbar = fig.colorbar(sc, ax=ax, aspect=100)
    #cbar.set_label('Температура (°C)')
    ax.set_xlabel('X координаты')
    ax.set_ylabel('Y координаты')
    ax.set_zlabel('Z координаты')
    
    fig.canvas.draw()
    return cbar,cbar2

