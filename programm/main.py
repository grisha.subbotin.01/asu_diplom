import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.interpolate import griddata
import tkinter as tk
from tkinter import filedialog
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.cm as cm
import tkinter.ttk as ttk

import parserdata
import plotter

def load_file():
    selected_parser = parser_combobox.get()
    if selected_parser == "csv":
        file_path = filedialog.askopenfilename(filetypes=[("CSV файлы", "*.csv")])
    else:
        file_path = filedialog.askopenfilename(filetypes=[("Текстовые файлы", "*.txt")])

    if file_path:
        with open(file_path, 'r') as file:
            data_str = file.read()
            if selected_parser == "custom":
                sensor_data = parserdata.parse_sensor_data_custom(data_str)
            elif selected_parser == "csv":
                sensor_data = parserdata.parse_sensor_data_csv(data_str)
            plotter.update_plot(sensor_data, ax, ax2, fig, cbar, cbar2)
            update_status_bar(f"Загружен файл {file_path}")

def about_author():
    about_window = tk.Toplevel(window)
    about_window.geometry("500x400")
    label = tk.Label(about_window, text="Программный комплекс для построения температурной карты \nthermoscope. \nАвтор: Субботин Григорий, выпускник АлтГу, ИЦТЭФ. \nРазрешено свободное распространение и модификация \nс указанием изначального автора. ",justify="center",font=("Arial",12))
    about_window.title("Об авторе")
    label.pack(expand=True,fill="both")

def save_plot():
    file_path = filedialog.asksaveasfilename(defaultextension=".png", filetypes=[("PNG файлы", "*.png")])
    if file_path:
        canvas.print_png(file_path)
        update_status_bar(f"Сохранено изображение {file_path}")
    else:
        update_status_bar("")

def update_status_bar(text):
    status_bar.config(text=text)

window = tk.Tk()
window.title("thermoscope")

# Создаем Frame для кнопок и combobox
button_frame = tk.Frame(window)
button_frame.pack(side=tk.TOP, fill=tk.X)


parser_label = tk.Label(button_frame, text="Тип данных:", font=("Arial", 10))
parser_label.pack(side=tk.LEFT, padx=10, pady=5)

# Создаем combobox в новом frame
parser_combobox = ttk.Combobox(button_frame, values=["custom", "csv"])
parser_combobox.current(0)  # выбираем вариант "custom" по умолчанию
parser_combobox.pack(side=tk.LEFT, padx=10, pady=10)

# Создаем кнопки и упаковываем их в Frame
load_button = tk.Button(button_frame, text="Загрузить", command=load_file)
about_button = tk.Button(button_frame, text="Об авторе", command=about_author)
save_button = tk.Button(button_frame, text="Сохранить", command=save_plot)
load_button.pack(side=tk.LEFT)
save_button.pack(side=tk.LEFT)
about_button.pack(side=tk.RIGHT)


# Создаем Frame для canvas
canvas_frame = tk.Frame(window)
canvas_frame.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(121, projection='3d')
ax2 = fig.add_subplot(122)

cbar = None
cbar2 = None

canvas = FigureCanvasTkAgg(fig, master=canvas_frame)
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

# Создаем Frame для статусбара
status_frame = tk.Frame(window)
status_frame.pack(side=tk.BOTTOM, fill=tk.X)
status_bar = tk.Label(status_frame, text="Программа thermoscope готова!", bd=1, relief=tk.SUNKEN, anchor=tk.W,font=("Aria",12))
status_bar.pack(fill=tk.BOTH)

window.mainloop()
