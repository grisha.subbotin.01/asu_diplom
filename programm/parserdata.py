import numpy as np
import csv
from io import StringIO 
def parse_sensor_data_custom(data_str):
    lines = data_str.strip().split('\n')
    sensor_data = []
    for line in lines:
        parts = line.split(';')
        sensor_id = None
        temp = None
        x = None
        y = None
        z = None
        for part in parts:
            if part.startswith('n:'):
                sensor_id = int(part.split(':')[1])
            elif part.split(':')[0] == 't':
                temp = float(part.split(':')[1])
            elif part.startswith('x:'):
                x = int(part.split(':')[1])
            elif part.startswith('y:'):
                y = int(part.split(':')[1])
            elif part.startswith('z:'):
                z = int(part.split(':')[1])
        sensor_data.append((sensor_id, temp, x, y, z))
    return np.array(sensor_data)

def parse_sensor_data_csv(data_str):
    sensor_data = []

    data_file_object = StringIO(data_str)

    reader = csv.DictReader(data_file_object)
    for row in reader:
        sensor_id = int(row['n'])  # преобразуем строку в целое число
        temp = float(row['t'])  # преобразуем строку в число с плавающей точкой
        x = float(row['x'])
        y = float(row['y'])
        z = float(row['z'])
        sensor_data.append((sensor_id, temp, x, y, z))

    return np.array(sensor_data)